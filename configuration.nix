{conifg, pkgs, ...}:

{
  imports = [ <nixpkgs/nixos/modules/virtualisation/amazon-image.nix> ];
  ec2.hvm = true;
  environment.systemPackages = with pkgs;
    [ tmux
      vim
      stack
      gitAndTools.gitFull
      nfs-utils
      mosh
    ];

  # Used to force filesystem resizing when EBS volume is expanded
  # Requires reboot to take effect
  boot.initrd.postDeviceCommands= ''
    ${pkgs.e2fsprogs}/sbin/e2fsck -fp /dev/disk/by-label/nixos
  '';

  # Required to download and build reflex
  nix.binaryCaches = [ "https://cache.nixos.org" "https://nixcache.reflex-frp.org" ];
  nix.binaryCachePublicKeys = [ "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];

  security.sudo = {
    enable = true;
    configFile = ''
      Defaults env_reset
      root ALL = (ALL:ALL) ALL
      %wheel ALL = (ALL) SETENV: NOPASSWD: ALL
    '';
  }; 

  nix.gc = {
    automatic = true;
    dates = "00:00";
  };

  # networking.firewall.allowedTCPPorts = [ 80 3000 22 ];
  # networking.firewall.allowedUDPPorts = [ 60000 60001 60002 ];
  # networking.firewall.extraCommands = ''
    # iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3000
  # '';

  users.extraUsers.ec2-user = {
             isNormalUser = true;
             home = "/home/ec2-user";
             description = "Admin user";
             extraGroups = [ "wheel" "networkmanager" ];
             uid = 500;
             openssh.authorizedKeys.keyFiles = [ ./keys/authorized_keys ];
  };

  fileSystems."/efs" = {
    device = "fs-517b9928.efs.us-east-2.amazonaws.com:/";
    fsType = "nfs4";
    options = [ 
      "nofail"
      "nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2"
    ];
  };

  swapDevices = [ { device = "/swapfile"; } ];

  # tmux basic configuration
  # Will be moved to project specific directories eventually
  environment.etc."tmux.conf".text = ''
    # split panes using | and -
    bind | split-window -h
    bind - split-window -v
    unbind '"'
    unbind %
    
    # reload config file
    bind r source-file /etc/tmux.conf

    # switch panes using alt-arrow without prefix
    bind -n M-Left select-pane -L
    bind -n M-Right select-pane -R
    bind -n M-Up select-pane -U
    bind -n M-Down select-pane -D
  '';
}

