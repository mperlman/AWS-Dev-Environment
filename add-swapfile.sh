#!/usr/bin/env bash

SWAPFILE=/swapfile
SIZE=16G

# Check if swapfile exists and create it if it doesn't
# Hard coded size of 16GB
if [ -e /swapfile ]
then
	echo "/swapfile already exists. No action taken."
	exit
fi

fallocate --length ${SIZE} ${SWAPFILE}
chmod 600 ${SWAPFILE}
mkswap ${SWAPFILE}
swapon ${SWAPFILE}
